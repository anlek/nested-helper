= AnlekNestedHelper

This project rocks and uses MIT-LICENSE.

== Methods added by this gem

``` ruby
  nested_objects(f, :images, {label: "Add more images"})
  # `f` is the form object
  # you'll need _image_fields.html partial (when sending :images)
  # options `label` is the label on the add button

  remove_nested_item(f, {}) #Your item must have a class of "item"
  generate_nested_template(f, method, {})
```